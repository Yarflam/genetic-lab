# Genetic Lab

Clone the project:

```bash
git clone https://gitlab.com/Yarflam/genetic-lab.git &&\
cd genetic-lab
```

## BNT162b2

Let's explore the source code of BioNTech / Pfizer vaccine against SARS-CoV-2

### Usages

**Show the data**

```bash
cd BNT162b2 &&\
node BNT162b2.js
```

### Sources

-   [berthub.eu (ES) - official](https://berthub.eu/articles/posts/ingenieria_inversa_del_codigo_fuente_de_la_vacuna_de_biontech_pfizer_para_el_sars-cov-2/)
-   [renaudguerin.net (FR)](https://renaudguerin.net/posts/explorons-le-code-source-du-vaccin-biontech-pfizer-sars-cov-2/)

## European Health Certificate

To explore the data stored in the European health pass.

### Install

```bash
cd ./EHC-Vaccin && npm i
```

### Usages

**Import EHC class**

```javascript
const EHC = require('/path/to/genetic-lab/EHC-Vaccin');
```

**Test with a dump**

```javascript
const cert = new EHC().loadDump('cert_dump');
console.log(cert.toString());
```

**Test with a QR code**

```javascript
(async () => {
    const cert = await new EHC().loadQrcode('cert_qrcode.jpg');
    console.log(cert.toString());
})();
```

**Generate a QR code**

```javascript
/* Initialize basic certificate */
const cert = new EHC().preset();

/* Fill data */
cert.loadObj({
    firstname: 'YARFLAM',
    lastname: 'GITLAB',
    birthdate: '2000-01-01',
    doseDate: '2021-07-01T00:00:00.000Z'
});

/* Generate the header */
cert.genID().genDates();

/*
 * Generate the controls
 * In real use: the document is certified by COSE signature
 */
cert.genHash().genSign();

/* Show the complete certificate */
console.log(cert.toString());

/* Save it */
cert.saveQrcode(require('path').resolve(__dirname, 'yarf_gitlab_ehc.jpg'));
```

**Example**

![YARFLAM-GITLAB-EHC](./EHC-Vaccin/yarf_gitlab_ehc.jpg)

```text
[[ Health Certificate Version 1 ]]

ID: urn:uvci:01:FR:NN3HPRNKP1L1#S
Hash: 1fd3c05694d51b19
Created		2021-07-14T23:25:41.000Z
Expiration	2023-06-05T22:00:00.000Z (v+60904800)

Firstname	YARFLAM
Lastname	GITLAB
Birthdate	2000-01-01
Country		FR
Doses		2 / 2
Last injection	2021-07-01

Disease		Covid-19
Vaccin		Comirnaty (covid-19 vaccines)
Organization	ORG-100030215

Signature: 84e0def00b20d5e3f9f5e4f38efb6a4f...
```

### Sources

-   [callicode.fr (FR)](https://blog.callicode.fr/post/2021/covid_pass.html)
-   [eHealth Network specifications (EN)](https://ec.europa.eu/health/sites/default/files/ehealth/docs/digital-green-certificates_dt-specifications_en.pdf)
-   [Union Register of medicinal products for human use (EN)](https://ec.europa.eu/health/documents/community-register/html/h1528.htm)
-   [COSE signature (EN)](https://www.iana.org/assignments/cose/cose.xhtml)
-   [Verify EHC (EN)](https://github.com/panzi/verify-ehc/blob/main/verify_ehc.py)
-   **Green Cert documents (EN)**
    -   [Green Cert v1](https://ec.europa.eu/health/sites/default/files/ehealth/docs/digital-green-certificates_v1_en.pdf): general explanations
    -   [Green Cert v2](https://ec.europa.eu/health/sites/default/files/ehealth/docs/digital-green-certificates_v2_en.pdf): certification design
    -   [Green Cert v3](https://ec.europa.eu/health/sites/default/files/ehealth/docs/digital-green-certificates_v3_en.pdf): the serialization process is well explained (page 9)
    -   [Green Cert v4](https://ec.europa.eu/health/sites/default/files/ehealth/docs/digital-green-certificates_v4_en.pdf): API certification process
    -   [Green Cert v5](https://ec.europa.eu/health/sites/default/files/ehealth/docs/digital-green-certificates_v5_en.pdf): cryptographic methods
