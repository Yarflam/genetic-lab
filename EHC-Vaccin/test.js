const EHC = require('./index');

(async () => {
    const qrname = 'yarf_gitlab_ehc.jpg';
    const qrpath = require('path').resolve(__dirname, qrname);
    const cert = await new EHC().loadQrcode(qrpath);
    console.log(cert.toString());
})();
