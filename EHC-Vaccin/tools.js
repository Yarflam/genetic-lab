const cbor = require('cbor');

/* Toolkit */
const tools = {
    /* Random */
    _alphaHex: 'abcdef0123456789',
    _alphaUcN: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
    rand: (a, b) => Math.random() * (b - a) + a,
    randInt: (a, b) => Math.floor(tools.rand(a, b)),
    randStr: (n, alpha) =>
        new Array(Math.max(1, n >> 0))
            .fill(0)
            .map(() => alpha[tools.randInt(0, alpha.length)])
            .join(''),
    randHex: n => tools.randStr(n, tools._alphaHex),
    randUcN: n => tools.randStr(n, tools._alphaUcN),
    // /* Buffer -> ArrayBuffer */
    // bfToAbf: buff => {
    //     let abf = new ArrayBuffer(buff.length);
    //     let view = new Uint8Array(abf);
    //     for (let i = 0; i < buff.length; ++i) view[i] = buff[i];
    //     return abf;
    // },
    // /* ArrayBuffer -> Buffer */
    // abfToBf: abf => {
    //     let buff = Buffer.alloc(abf.byteLength);
    //     let view = new Uint8Array(abf);
    //     for (let i = 0; i < buff.length; ++i) buff[i] = view[i];
    //     return buff;
    // },
    /* Object -> Map */
    objToMap: obj =>
        new Map(
            Object.entries(obj).map(([key, value]) => [
                typeof key === 'string' && /^-?[0-9]+$/.test(key)
                    ? Number(key)
                    : key,
                value
            ])
        ),
    /* CBOR */
    cborEncode: obj => {
        return cbor.encode(obj);
    },
    cborDecode: data => {
        /* Map object - decode */
        const dMap = obj => {
            if (obj instanceof Map) {
                return [...obj.keys()].reduce((accum, key) => {
                    accum[key] = dMap(obj.get(key));
                    return accum;
                }, {});
            }
            return obj;
        };
        /* Destructuring */
        let tag = cbor.decode(data);
        tag.value = tag.value.map((entry, i) => {
            try {
                return dMap(cbor.decode(entry));
            } catch (e) {
                return entry;
            }
        });
        return tag;
    }
};

module.exports = tools;
