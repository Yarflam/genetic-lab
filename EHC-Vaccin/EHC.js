const QrCodeReader = require('qrcode-reader');
const QrCodeWriter = require('qrcode');
const base45 = require('base45');
const Jimp = require('jimp');
const zlib = require('zlib');
const fs = require('fs');

const tools = require('./tools');

/* European Health Certificate */
class EHC {
    constructor() {
        this._rawData = null;
        /* Data */
        this._hcv = null; // Health Certificate Version
        this._encType = null; // Encoding Type
        this._hash = null; // Hash Document
        this._orga = null; // Issuing Organization (ex: CNAM)
        this._dateEmit = null; // Date of issue
        this._dateExpire = null; // Expiry date
        this._version = null; // Version
        this._firstname = null; // Firstname
        this._lastname = null; // Lastname
        this._birthdate = null; // Birthdate
        this._id = null; // Document ID
        this._disease = null; // Disease code
        this._vaccin = null; // Type of vaccine
        this._country = null; // Country initial
        this._reqDose = null; // Number of doses required
        this._doseNb = null; // Number of doses injected
        this._doseDate = null; // Date of last injection
        this._manufactName = null; // Name of vaccine manufacturer
        this._manufactReg = null; // Union Register of medicinal products
        this._sign = null; // Signature
    }

    toString() {
        const emptyDate = new Date(0);
        return `[[ Health Certificate Version ${this._hcv} ]]\n\nID: ${
            this._id
        }\nHash: ${this._hash}\nCreated\t\t${(
            this._dateEmit || emptyDate
        ).toISOString()}\nExpiration\t${(
            this._dateExpire || emptyDate
        ).toISOString()} (v+${((this._dateExpire || emptyDate) -
            (this._doseDate || emptyDate)) /
            1000})\n\nFirstname\t${this._firstname}\nLastname\t${
            this._lastname
        }\nBirthdate\t${
            (this._birthdate || emptyDate).toISOString().split('T')[0]
        }\nCountry\t\t${this._country}\nDoses\t\t${this._doseNb} / ${
            this._reqDose
        }\nLast injection\t${
            (this._doseDate || emptyDate).toISOString().split('T')[0]
        }\n\nDisease\t\t${EHC.BASE.DISEASES[this._disease] ||
            this._disease}\nVaccin\t\t${EHC.BASE.VACCINMEDS[
            this._manufactReg
        ] || this._manufactReg} (${EHC.BASE.VACCINS[this._vaccin] ||
            this._vaccin})\nOrganization\t${this._manufactName}\n\nSignature: ${
            this._sign
        }`;
    }

    /* Load the default values (preset) */
    preset() {
        const preset = EHC.DEFAULT;
        this._hcv = preset.HCV;
        this._encType = preset.ENCTYPE;
        this._disease = preset.DISEASE;
        this._vaccin = preset.VACCINS;
        this._country = preset.COUNTRY;
        this._reqDose = preset.DOSES;
        this._doseNb = preset.DOSES;
        this._manufactName = preset.MANUFACTURER;
        this._manufactReg = preset.VACCINMEDS;
        return this;
    }

    /* Load QR code */
    async loadQrcode(path) {
        await new Promise(resolve => {
            /* Open the image */
            Jimp.read(fs.readFileSync(path), (err, img) => {
                if (err) return resolve(false);
                /* Decode QR code */
                let qr = new QrCodeReader();
                qr.callback = (err, value) => {
                    if (err) return resolve(false);
                    /* Load the data */
                    this.loadRawData(this._decode(value.result));
                    resolve(true);
                };
                qr.decode(img.bitmap);
            });
        });
        return this;
    }

    /* Load Dump file */
    loadDump(path) {
        this.loadRawData(this._decode(fs.readFileSync(path).toString()));
        return this;
    }

    /* Load Object data */
    loadObj(obj) {
        if (typeof obj !== 'object' || obj === null) return this;
        Object.keys(obj).forEach(key => {
            if (typeof this[`_${key}`] === 'undefined') return;
            this[`_${key}`] = /date/i.test(key) ? new Date(obj[key]) : obj[key];
        });
        return this;
    }

    loadRawData(data) {
        this._rawData = data;
        this._hcv = this._rawData.hcv;
        /* Header */
        this._encType = this._rawData.header['1'];
        this._hash = this._rawData.header['4'].toString('hex');
        /* Payload */
        this._orga = this._rawData.payload['1'];
        this._dateExpire = new Date(this._rawData.payload['4'] * 1000);
        this._dateEmit = new Date(this._rawData.payload['6'] * 1000);
        /* Payload -> Patient */
        const patient = (this._rawData.payload['-260'] || {})['1'] || {};
        this._version = patient.ver;
        this._firstname = patient.nam.gn || patient.nam.gnt;
        this._lastname = patient.nam.fn || patient.nam.fnt;
        this._birthdate = new Date(patient.dob);
        /* Payload -> Document */
        const doc = (patient.v || [])[0] || {};
        this._id = doc.ci;
        this._disease = doc.tg;
        this._vaccin = doc.vp;
        this._country = doc.co;
        this._reqDose = doc.sd;
        this._doseNb = doc.dn;
        this._doseDate = new Date(doc.dt);
        this._manufactName = doc.ma;
        this._manufactReg = doc.mp;
        this._orga = this._orga || doc.is; // same
        /* Signature */
        this._sign = this._rawData.sign.toString('hex');
        return this;
    }

    _decode(sData) {
        if (!/^HC[1-2]:/.test(sData)) return null;

        /* Health Certificate Version 1/2 */
        const hcv = sData.match(/^HC([1-2]):/)[1] >> 0;
        sData = sData.substr(4);

        /* Base45 decode */
        let bData = base45.decode(sData);
        /* Zlib uncompress */
        bData = zlib.unzipSync(bData);
        /* Cbor parse data */
        bData = tools.cborDecode(bData);
        let [header, , payload, sign] = bData.value;

        /* Treatment */
        return {
            hcv,
            header,
            payload,
            sign
        };
    }

    _encode() {
        const rawData = this._getRawData();

        /* Cbor encode */
        rawData.payload['-260'] = tools.objToMap(rawData.payload['-260']);
        let bData = tools.cborEncode([
            tools.cborEncode(tools.objToMap(rawData.header)),
            {},
            tools.cborEncode(tools.objToMap(rawData.payload)),
            rawData.sign
        ]);
        bData = Buffer.from('d2' + bData.toString('hex'), 'hex');
        /* Zlib compress */
        bData = zlib.deflateSync(bData);
        /* Base45 encode */
        let sData = base45.encode(bData);

        /* Return the chain */
        return `HC${rawData.hcv}:${sData}`;
    }

    _getRawData() {
        return {
            hcv: this._hcv,
            header: {
                '1': this._encType,
                '4': Buffer.from(this._hash, 'hex')
            },
            payload: {
                '1': this._orga,
                '4': Math.floor(this._dateExpire.getTime() / 1000),
                '6': Math.floor(this._dateEmit.getTime() / 1000),
                '-260': {
                    '1': {
                        v: [
                            {
                                ci: this._id,
                                co: this._country,
                                dn: this._doseNb,
                                dt: new Date(this._doseDate)
                                    .toISOString()
                                    .split('T')[0],
                                is: this._orga,
                                ma: this._manufactName,
                                mp: this._manufactReg,
                                sd: this._reqDose,
                                tg: this._disease,
                                vp: this._vaccin
                            }
                        ],
                        dob: new Date(this._birthdate)
                            .toISOString()
                            .split('T')[0],
                        nam: {
                            fn: this._lastname.toUpperCase(),
                            gn: this._firstname.toUpperCase(),
                            fnt: this._lastname.toUpperCase(),
                            gnt: this._firstname.toUpperCase()
                        },
                        ver: this._version
                    }
                }
            },
            sign: Buffer.from(this._sign, 'hex')
        };
    }

    /* Save */

    saveQrcode(path) {
        QrCodeWriter.toFile(path, [
            {
                data: this._encode(),
                mode: 'byte'
            }
        ]);
    }

    /* Generators */

    genID() {
        this._id = `urn:uvci:${EHC.DEFAULT.BASEID}:${
            this._country
        }:${tools.randUcN(EHC.RAND.ID[0])}#${tools.randUcN(EHC.RAND.ID[1])}`;
        return this;
    }

    genDates() {
        this._dateEmit = new Date(); // now
        this._dateExpire = new Date(
            new Date(
                (this._doseDate || new Date(0)).toISOString().split('T')[0]
            ).getTime() +
                EHC.DEFAULT.EXPTIME * 1000
        );
        return this;
    }

    genHash() {
        this._hash = tools.randHex(EHC.RAND.HASH);
        return this;
    }

    genSign() {
        this._sign = tools.randHex(EHC.RAND.SIGN);
        return this;
    }
}

EHC.RAND = {
    HASH: 16,
    SIGN: 128,
    ID: [12, 1]
};

EHC.DEFAULT = {
    HCV: 1, // Health Certificate Version 1
    ENCTYPE: -7, // ECDSA 256
    DISEASE: '840539006', // Covid19
    VACCINS: 'J07BX03', // Covid-19 vaccines
    VACCINMEDS: 'EU/1/20/1528', // Comirnaty
    MANUFACTURER: 'ORG-100030215', // Comirnaty Organization
    COUNTRY: 'FR', // Origin: France
    DOSES: 2, // Nb doses injected & required
    BASEID: '01',
    EXPTIME: 60904800 // Document expiration time after injection
};

EHC.BASE = {
    DISEASES: {
        '840539006': 'Covid-19'
    },
    VACCINS: {
        '1119305005': 'SARS-CoV2 antigen vaccine',
        '1119349007': 'SARS-CoV2 mRNA vaccine',
        J07BX03: 'covid-19 vaccines'
    },
    VACCINMEDS: {
        'EU/1/20/1528': 'Comirnaty',
        'EU/1/20/1507': 'COVID-19 Vaccine Moderna',
        'EU/1/21/1529': 'Vaxzevria',
        'EU/1/20/1525': 'COVID-19 Vaccine Janssen',
        CVnCoV: 'CVnCoV',
        NVXCoV2373: 'NVXCoV2373',
        'Sputnik-V': 'Sputnik V',
        Convidecia: 'Convidecia',
        EpiVacCorona: 'EpiVacCorona',
        'BBIBP-CorV': 'BBIBPCorV',
        'InactivatedSARS-CoV-2-Vero-Cell': 'Inactivated SARSCoV-2',
        CoronaVac: 'CoronaVac',
        Covaxin: 'Covaxin'
    }
};

module.exports = EHC;
