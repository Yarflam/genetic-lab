const fs = require('fs');

/* Tab */
const TAB = '    ';

/* Transcription */
const codons = Object.entries(require('./codons.json'));
const codonsIni = require('./codons-initial.json');

/* Helpers */
const showLongCode = code => code.replace(/(([^ ]+( |$)){20})/g, `$1\n${TAB}`);
const splitGen = code => code.replace(/([ACGTU]{3})/g, '$1 ');
const splitBigGen = code => showLongCode(splitGen(code));
const rnaToProtein = rna =>
    splitGen(rna.replace(/U/g, 'T'))
        .split(' ')
        .map(elem =>
            codons
                .filter(([key, bases]) => bases.indexOf(elem) >= 0)
                .map(([key]) => `${codonsIni[key]}/${key}`.toUpperCase())
        )
        .join(' ');

/*
 * BNT162b2 - Sequence
 */
let seq = fs.readFileSync('BNT162b2.txt', 'utf8');
seq = seq.replace(/[^ACGTUΨ]/g, '');

/* HACKING : Ψ -> 1-methyl-3'-pseudouridylyl -> U */
seq = seq.replace(/Ψ/g, 'U');

/* (1) Cap -- Start the instructions */
const cap = seq.slice(0, 2);
seq = seq.slice(2);
console.log(`.cap\n${TAB}${cap}\n`);

/* (2) 5'-UTR -- Reading sheet the ribosome */
const fiveUtr = seq.slice(0, 52);
seq = seq.slice(52);
console.log(`.5utr\n${TAB}${splitBigGen(fiveUtr)}\n`);

/* (3) sig -- S-glycoprotein signal peptide */
const sig = seq.slice(0, 48);
seq = seq.slice(48);
console.log(`.sig\n${TAB}${splitGen(sig)}`);
console.log(`@RNA\n${TAB}${showLongCode(rnaToProtein(sig))}\n`);

/* (4) S protein_mut -- Spike */
const spike = seq.slice(0, 3777);
seq = seq.slice(3777);
console.log(`.spike\n${TAB}${splitBigGen(spike)}`);
console.log(`@RNA\n${TAB}${showLongCode(rnaToProtein(spike))}\n`);

/* (5) 3'-UTR -- Helps promote protein expression */
const utrThree = seq.slice(0, 295);
seq = seq.slice(295);
console.log(`.3utr\n${TAB}${splitBigGen(utrThree)}\n`);

/* (6) poly(A) -- Protection against degradation */
const polyA = String(seq);
console.log(`.polyA\n${TAB}${splitBigGen(polyA)}\n`);
