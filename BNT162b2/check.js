/* Check Codons */
const cds = require('./codons.json');
const cdsName = require('./codons-name.json');

const cdsList = Object.values(cds)
    .reduce((accum, items) => accum.concat(items), [])
    .sort((a, b) => (a < b ? -1 : 1));

/* Duplicates */
console.log(cdsList.filter((item, index) => cdsList.indexOf(item) !== index));

/* Incomplete */
const bf = (alpha = 'ACGT', n = 3) =>
    new Array(Math.pow(alpha.length, n)).fill(0).map((_, x) =>
        new Array(n)
            .fill(0)
            .map(
                (_, k) =>
                    alpha[
                        Math.floor(
                            (x % Math.pow(alpha.length, k + 1)) /
                                Math.pow(alpha.length, k)
                        )
                    ]
            )
            .reverse()
            .join('')
    );
console.log(bf().filter(x => cdsList.indexOf(x) < 0));

/* List */
Object.entries(cdsName).forEach(([key, name]) => {
    console.log(
        `[${key}] ${name}\n${cds[key].map(x => `  - ${x}`).join('\n')}\n`
    );
});
